<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <form action="{{ route('formeditpost', ['id' => $book->id]) }}" method="post">
            @csrf
            <input type="text" name="judul" placeholder="Judul Buku" value="{{ $book->judul }}"> <br>
            <input type="text" name="pengarang" placeholder="Pengarang Buku" value="{{ $book->pengarang }}"> <br>
            <button type="submit">Kirim</button>
        </form>
    </body>
</html>
