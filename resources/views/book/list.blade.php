<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>List Book</title>
    </head>
    <body>
        <table border="1">
            <tr>
                <th>No</th>
                <th>Judul</th>
                <th>Pengarang</th>
                <th>Aksi</th>
            </tr>

            @foreach($book as $index => $data)
            <tr>
                <td>{{ $index+1 }}</td>
                <td>{{ $data->judul }}</td>
                <td>{{ $data->pengarang }}</td>
                <td>
                    <a href="{{ route('formedit', ['id' => $data->id]) }}">Edit</a>
                </td>
            </tr>
            @endforeach

        </table>
    </body>
</html>
